package com.battcn.platform.config.shiro;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * shiro 提供了 CacheManager 接口，这样一来只要我们实现自己的 CacheManager 就可以用各种缓存进行操作了
 *
 * @author Levin
 * @since 2018/11/27 0027
 */
public class ShiroRedisCacheManager implements CacheManager {

    private long expire = 30 * 60 * 1000;
    private String keyPrefix = "shiro-session:";

    public ShiroRedisCacheManager(long expire, String keyPrefix) {
        this.expire = expire;
        this.keyPrefix = keyPrefix;
    }

    @Resource
    private RedisTemplate<Serializable, Session> redisSessionTemplate;

    @SuppressWarnings("unchecked")
    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        ShiroCache cache = new ShiroCache(expire, keyPrefix, redisSessionTemplate);
        return (Cache<K, V>) cache;
    }

}
